package com.innominds.microservice.currencyexchangeservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.innominds.microservice.currencyexchangeservice.repository.ExchangeValueRepository;

@RestController
public class CurrencyExchangeController {

	@Autowired
	Environment environment;

	@Autowired
	private ExchangeValueRepository repository;

	@GetMapping("/currency-exchange/from/{from}/to/{to}")
	public com.innominds.microservice.currencyexchangeservice.entity.ExchangeValue retrieveExchangeValue(
			@PathVariable String from, @PathVariable String to) {

		// ExchangeValue exchangeValue = new ExchangeValue(1000L, from, to, new
		// BigDecimal("76.66"));

		com.innominds.microservice.currencyexchangeservice.entity.ExchangeValue exchangeValue = repository
				.findByFromAndTo(from, to);

		exchangeValue.setPort(Integer.parseInt(environment.getProperty("local.server.port")));

		return exchangeValue;

	}
}
